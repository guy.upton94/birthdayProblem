package production;

import java.util.Arrays;

public class Party {
	
	private final int noChild;
	private Child[] theChildren;
	
	public Party(int noChild) {
		this.noChild = noChild;
		
		 theChildren = new Child[noChild];
		
		for(int i = 0; i < noChild; i++) {
			theChildren[i] = new Child();
		}
		
	}
	
	public boolean findMatch() {
		
		Child[] childrenCopy = new Child[noChild];
		
		for(int i = 0; i < noChild; i++) {
			childrenCopy[i] = theChildren[i];
		}
		
		Arrays.sort(childrenCopy, new DayCompare());
		
		
		for(int i = 0; i < (noChild-1); i++) {
			if(childrenCopy[i].getDay() == childrenCopy[i+1].getDay()) {
				return true;
			}
		}
		
		return false;
		
	}
	

}
