package production;

import java.util.Comparator;

public class DayCompare implements Comparator<Child>{
	public int compare(Child lhs, Child rhs) {
		//System.out.println(1);
		return lhs.getDay() - rhs.getDay();
	}
}