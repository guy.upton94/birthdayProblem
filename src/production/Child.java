package production;


import java.util.Random;

public class Child {
	
	private final int day;

	
	
	public Child() {
		Random rand = new Random();
		
		this.day = rand.nextInt(365);

	}
	
	public Child(int day) {
		
		this.day = day;
	}



	@Override
	public String toString() {
		return "Child [day=" + day + "]";
	}



	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Child other = (Child) obj;
		if (day != other.day)
			return false;
		return true;
	}



}





