package testing;

import java.text.DecimalFormat;

import production.Party;

public class Test {
	
	
	public static void main(String [] args){
		
		for(int i = Constants.MIN_CHILDREN; i < Constants.MAX_CHILDREN+1; i++) {
			int winTotal = 0;
			for(int j = 0; j < Constants.REPEATS; j++) {
				if(new Party(i).findMatch()) {
					winTotal++;
				}
			}
			
			
			DecimalFormat df = new DecimalFormat("####0.00");
			double percentage = ((winTotal / (double) Constants.REPEATS)) * 100;
			System.out.println(i + " Children gave a win rate of:\t" + df.format(percentage) + "%");
			
		}
		
		
	}

}
