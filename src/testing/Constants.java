package testing;

public class Constants {
	
	
	public static final int MIN_CHILDREN = 1;
	public static final int MAX_CHILDREN = 50;
	
	public static final int REPEATS = 1000;
	
	public static final int MAX_DAYS = 365;
	
}
