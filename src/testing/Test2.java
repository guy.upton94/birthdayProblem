package testing;

import java.util.Arrays;

import production.Child;
import production.DayCompare;

public class Test2 {
	
	
	public static void main(String [] args){
		
		
		
		int[] days = {0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,19,20};
		int noChild = days.length;
		
		Child[] childrenCopy = new Child[noChild];
		
		for(int i = 0; i < days.length; i++) {
			childrenCopy[i] = new Child(days[i]);
		}
		
		Arrays.sort(childrenCopy, new DayCompare());
		
		
		for(int i = 0; i < (noChild-1); i++) {
			
			if(childrenCopy[i].getDay() == childrenCopy[i+1].getDay()) {
				System.out.println("Found");
			}
		}
		
	}

}
